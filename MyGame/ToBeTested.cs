namespace MyGame
{
    public class ToBeTested {
        public static int AddOne(int x) {
            if(x >= 5){
                return x + 1;
            } else {
                return x - 1;
            }
        }
    }
}